package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/teste-lyncas/controller"
)

func ajustaRotas(r *gin.Engine) {
	api := r.Group("/api")
	{
		api.GET("/books", controller.GetBooks)
		api.POST("/book/:id/favorite", controller.PostFavoriteBook)
		api.GET("/book/favorites", controller.GetFavoritesBook)
		api.DELETE("/book/:id/favorite", controller.DeleteFavoriteBook)
	}
}

func main() {
	r := gin.Default()
	ajustaRotas(r)
	r.Run(":8080")
}
