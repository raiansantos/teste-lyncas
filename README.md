# INSTRUÇÕES

A API contém os 4 endpoints solicitados:
- GET /api/books?p={term} onde term é o termo de pesquisa. A paginação é alcançada usando os parametros skip e limit
- POST /api/book/{id}/favorite
- GET /api/book/favorites - retornar uma lista de favoritos
- DELETE /api/book/{id}/favorite - excluir um favorito

Para execução deve ser fazer o clone do projeto, e executar no diretório raiz o comando:
```
go run main.go
```

Com a execução do projeto é possível acessar os endpoints usando a URL base http://localhost:8080/